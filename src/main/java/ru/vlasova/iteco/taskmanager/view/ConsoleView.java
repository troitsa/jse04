package ru.vlasova.iteco.taskmanager.view;

import ru.vlasova.iteco.taskmanager.entity.Project;
import ru.vlasova.iteco.taskmanager.entity.Task;
import ru.vlasova.iteco.taskmanager.service.ProjectService;
import ru.vlasova.iteco.taskmanager.service.TaskService;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;

public class ConsoleView {

    private ProjectService projectService;

    private TaskService taskService;

    public ConsoleView(ProjectService projectService, TaskService taskService) {
        this.taskService = taskService;
        this.projectService = projectService;
        taskService.setProjectService(projectService);
        projectService.setTaskService(taskService);
    }

    public void clearProject() {
        projectService.clearProjects();
    }

    public void createProject(BufferedReader br) throws IOException {
        System.out.println("Creating project. Set name: ");
        String name = br.readLine();
        System.out.println("Input description: ");
        String description = br.readLine();
        System.out.println("Set start date: ");
        String dateStart = br.readLine();
        System.out.println("Set end date: ");
        String dateFinish = br.readLine();
        projectService.create(name, description, dateStart,dateFinish);
        System.out.println("Project created.");
    }

    public void printProjects() {
        List<Project> projectList = projectService.getProjects();
        if(projectList != null && projectList.size()!=0) {
            int i = 1;
            for (Project project : projectList) {
                System.out.println(i + ": " + project);
                i++;
            }
        }
    }

    public void editProject(BufferedReader br) throws IOException {
        System.out.println("Choose the task and type the number");
        printProjects();
        int index = Integer.parseInt(br.readLine())-1;
        Project project = projectService.getProject(index);
        if (project != null) {
            System.out.println("Editing task: " + project.getName() + ". Set new name: ");
            project.setName(br.readLine());
            System.out.println("Task edit.");
        }
    }

    public void removeProject(BufferedReader br) throws IOException {
        System.out.println("To delete project choose the project and type the number");
        printProjects();
        int index = Integer.parseInt(br.readLine())-1;
        projectService.remove(index);
        System.out.println("Project delete.");
    }

    public void clearTask() {
        taskService.clearTasks();
    }

    public void createTask(BufferedReader br) throws IOException {
        System.out.println("Creating task. Set name: ");
        String name = br.readLine();
        System.out.println("Input description: ");
        String description = br.readLine();
        System.out.println("Set start date: ");
        String dateStart = br.readLine();
        System.out.println("Set end date: ");
        String dateFinish = br.readLine();
        taskService.create(name, description, dateStart,dateFinish);
        System.out.println("Task created.");
    }

    public void printTasks(BufferedReader br) throws IOException {
        System.out.println("Please, choose the project and type the number");
        printProjects();
        int index = Integer.parseInt(br.readLine())-1;
        List<Task> taskList = taskService.getTasks(index);
        if(taskList.size()==0) {
            System.out.println("There are no tasks.");
        }
        else {
            printTaskList(taskList);
        }
    }

    public void printTaskList(List<Task> taskList) throws IOException {
        int i = 1;
        for(Task task : taskList) {
            System.out.println(i + ": " + task);
            i++;
        }
    }

    public void editTask(BufferedReader br) throws IOException {
        System.out.println("Choose the task and type the number");
        List<Task> taskList = taskService.getTasks();
        if (taskList.size() !=0) {
            printTaskList(taskList);
            int index = Integer.parseInt(br.readLine()) - 1;
            Task task = taskService.getTask(index);
            if (task != null) {
                System.out.println("Editing task: " + task.getName() + ". Set new name: ");
                task.setName(br.readLine());
                System.out.println("Set new description: ");
                task.setDescription(br.readLine());
                System.out.println("Choose project id: ");
                printProjects();
                int projectIndex = Integer.parseInt(br.readLine())-1;
                task.setProjectId(projectService.getProject(projectIndex).getId());
                System.out.println("Task edit.");
            }
        }
    }

    public void removeTask(BufferedReader br) throws IOException {
        System.out.println("To delete task choose the task and type the number");
        printTaskList(taskService.getTasks());
        int index = Integer.parseInt(br.readLine())-1;
        taskService.remove(index);
        System.out.println("Project delete.");
    }

    public void help() {
        System.out.println(" =====HELP=====\n" +
                "project_clear: Remove all projects.\n" +
                "project_create: Create new project.\n" +
                "project_list: Show all projects.\n" +
                "project_edit: Edit selected project\n" +
                "project_remove: Remove selected project\n" +
                "task_clear: Remove all tasks.\n" +
                "task_create: Create new task.\n" +
                "task_list: Show all tasks in project.\n" +
                "task_edit: Edit selected task.\n" +
                "task_remove: Remove selected task.\n" +
                "======================");
    }

}
