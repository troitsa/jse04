package ru.vlasova.iteco.taskmanager.repository;

import ru.vlasova.iteco.taskmanager.entity.Project;

import java.util.*;

public class ProjectRepository implements IRepository<Project> {

    private Map<String, Project> projects = new HashMap();

    @Override
    public List<Project> findAll() {
        return new ArrayList<>(projects.values());
    }

    @Override
    public Project findOne(String id) {
        return projects.get(id);
    }

    @Override
    public void persist(Project project) throws Exception {
        if (!projects.containsKey(project.getId())) {
            merge(project);
        } else {
            throw new Exception("Such project exists.");
        }
    }

    @Override
    public void merge(Project project) {
        projects.put(project.getId(), project);
    }

    @Override
    public void remove(String id) {
        projects.remove(id);
    }

    @Override
    public void removeAll() {
        projects.clear();
    }

}
