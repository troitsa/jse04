package ru.vlasova.iteco.taskmanager.repository;

import ru.vlasova.iteco.taskmanager.entity.Task;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TaskRepository implements IRepository<Task> {

    private Map<String, Task> tasks = new HashMap<>();

    @Override
    public List<Task> findAll() {
        return new ArrayList<Task>(tasks.values());
    }

    @Override
    public Task findOne(String id) {
        return tasks.get(id);
    }

    @Override
    public void persist(Task task) throws Exception {
        if (!tasks.containsKey(task.getId())) {
            merge(task);
        } else {
            throw new Exception("Such task exists.");
        }
    }

    @Override
    public void merge(Task task) {
        tasks.put(task.getId(), task);
    }

    @Override
    public void remove(String id) {
        tasks.remove(id);
    }

    @Override
    public void removeAll() {
        tasks.clear();
    }

}
