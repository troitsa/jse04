package ru.vlasova.iteco.taskmanager.repository;

import java.util.List;

public interface IRepository<T> {

    List<T> findAll();

    T findOne(String id);

    void persist(T obj) throws Exception;

    void merge(T obj);

    void remove(String id);

    void removeAll();

}
