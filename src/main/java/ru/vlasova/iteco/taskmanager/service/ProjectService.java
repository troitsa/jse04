package ru.vlasova.iteco.taskmanager.service;

import ru.vlasova.iteco.taskmanager.entity.Project;
import ru.vlasova.iteco.taskmanager.repository.ProjectRepository;
import ru.vlasova.iteco.taskmanager.util.DateUtil;

import java.util.List;

public class ProjectService extends AbstractService {

    ProjectRepository projectRepository;

    TaskService taskService;

    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public void setTaskService(TaskService taskService) {
        this.taskService = taskService;
    }

    public void create(String name, String description, String dateStart, String dateFinish) {
        boolean check = checkNull(name, description, dateStart, dateFinish);
        if (check) {
            Project project = new Project(name, description, DateUtil.parseDateFromString(dateStart), DateUtil.parseDateFromString(dateFinish));
            if (project != null) {
                try {
                    projectRepository.persist(project);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public List<Project> getProjects() {
        List<Project> projects = projectRepository.findAll();
        return projects;
    }

    public void remove(int index) {
        Project project = getProjectByIndex(index);
        projectRepository.remove(project.getId());
        taskService.removeTasksByProject(project);
    }

    public Project getProject(int index) {
        return getProjectByIndex(index);
    }

    public void clearProjects() {
        projectRepository.removeAll();
    }

    public Project getProjectByIndex(int index) {
        return projectRepository.findAll().get(index);
    }

}
