package ru.vlasova.iteco.taskmanager.service;

public abstract class AbstractService {

    public boolean checkNull(String name, String description, String dateStart, String dateFinish) {
        boolean check = (name != null) && (description != null) && (dateStart != null) &&
                (dateFinish != null) && (name.trim().length() != 0) && (description.trim().length() != 0) &&
                (dateStart.trim().length() != 0) && (dateFinish.trim().length() != 0);
        return check;
    }

}
