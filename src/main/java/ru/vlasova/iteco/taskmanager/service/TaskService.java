package ru.vlasova.iteco.taskmanager.service;

import ru.vlasova.iteco.taskmanager.entity.Project;
import ru.vlasova.iteco.taskmanager.entity.Task;
import ru.vlasova.iteco.taskmanager.repository.TaskRepository;
import ru.vlasova.iteco.taskmanager.util.DateUtil;

import java.util.ArrayList;
import java.util.List;

public class TaskService extends AbstractService {

    private TaskRepository taskRepository;

    private ProjectService projectService;

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public void setProjectService(ProjectService projectService) {
        this.projectService = projectService;
    }

    public void create(String name, String description, String dateStart, String dateFinish) {
        boolean check = checkNull(name, description, dateStart, dateFinish);
        if (check) {
            Task task = new Task(name, description, DateUtil.parseDateFromString(dateStart), DateUtil.parseDateFromString(dateFinish));
            if (task != null) {
                try {
                    taskRepository.persist(task);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public List<Task> getTasks(int projectIndex) {
        List<Project> projectList = projectService.getProjects();
        String projectId = projectList.get(projectIndex).getId();
        List<Task> tasks = getTasksByProject(projectId);
        return tasks;
    }

    public List<Task> getTasks() {
        List<Task> tasks = taskRepository.findAll();
        return tasks;
    }

    public void remove(Task task) {
        taskRepository.remove(task.getId());
    }

    public void remove(int id) {
        Task task = getTask(id);
        remove(task);
    }

    public Task getTask(int index) {
        return getTaskByIndex(index);
    }

    public void removeTasksByProject(Project project) {
        List<Task> taskList = getTasksByProject(project.getId());

        for (Task task : taskList) {
            taskRepository.remove(task.getId());
        }
    }

    public void clearTasks() {
        taskRepository.removeAll();
    }

    public int countTask() {
        return taskRepository.findAll().size();
    }

    public int countTaskByProject(Project project) {
        return getTasksByProject(project.getId()).size();
    }

    public Task getTaskByIndex(int index) {
        return taskRepository.findAll().get(index);
    }

    public List<Task> getTasksByProject(String projectId) {
        List<Task> listTasks = new ArrayList<>();
        List<Task> tasks = taskRepository.findAll();
        for (Task task : tasks) {
            if (task.getProjectId().equals(projectId)) {
                listTasks.add(task);
            }
        }
        return listTasks;
    }

}
