package ru.vlasova.iteco.taskmanager;

import ru.vlasova.iteco.taskmanager.context.Bootstrap;

public class App {

    public static void main(String[] args) {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.init();
    }

}