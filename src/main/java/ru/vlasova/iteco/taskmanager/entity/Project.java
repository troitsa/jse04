package ru.vlasova.iteco.taskmanager.entity;

import java.util.Date;
import java.util.UUID;

public class Project {

    private String id = UUID.randomUUID().toString();

    private String name = "";

    private String description = "";

    private Date dateStart;

    private Date dateFinish;

    public Project() {
    }

    public String getId() {
        return id;
    }

    public Project(String name, String description, Date dateStart, Date dateFinish) {
        this.name = name;
        this.description = description;
        this.dateStart = dateStart;
        this.dateFinish = dateFinish;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateFinish() {
        return dateFinish;
    }

    public void setDateFinish(Date dateFinish) {
        this.dateFinish = dateFinish;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Project project = (Project) o;
        return name.equals(project.getName());
    }

    @Override
    public String toString() {
        return "Project " + name;
    }

}
